package com.ezzahi.app;

import lombok.Builder;

import java.util.Map;
@Builder
public record ImageRequest(
        Map<String,String> metadata,
        String path,
        String fileName,
        byte[] data
) {
}
