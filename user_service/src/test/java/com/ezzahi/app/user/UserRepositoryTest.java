package com.ezzahi.app.user;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Optional;

import static com.ezzahi.app.user.Role.USER;
import static org.assertj.core.api.Assertions.*;

@DataJpaTest(
        properties = {
                "spring.properties.javax.persistence.validation.mode = none"
        }
)
public class UserRepositoryTest {

    @Autowired
    private UserRepository underTest;

    @Test
    void itShouldSelectTheUserByEmailWhenUserExist() {
        //given
        String userEmail = "hamzaezzahi@gmail.com";
        User user = User.builder()
                .name("hamza")
                .email(userEmail)
                .password("password")
                .role(USER)
                .build();
        underTest.save(user);
        //when
        Optional<User> optionalUser = underTest.findByEmail(userEmail);
        //then
        assertThat(optionalUser).isPresent()
                .hasValueSatisfying(c->c.getName().equals(user.getName()));
    }

    @Test
    void itShouldNotSelectTheUserByEmailWhenUserNotExist() {
        //given
        String userEmail = "hamzaezzahi@gmail.com";
        //when
        Optional<User> optionalUser = underTest.findByEmail(userEmail);
        //then
        assertThat(optionalUser).isNotPresent();
    }

    @Test
    void itShouldSaveTheUser() {
        //given
        String userEmail = "hamzaezzahi@gmail.com";
        User user = User.builder()
                .name("gamza")
                .email(userEmail)
                .password("password")
                .role(USER)
                .build();
        //when
        //then
        assertThatNoException().isThrownBy(()-> underTest.save(user));
    }

    @Test
    void itShouldNoteSaveTheUserWhenTheEmailIsNull() {
        //given
        User user = User.builder()
                .name("hamza")
                .email(null)
                .password("password")
                .role(USER)
                .build();
        //when
        //then
        assertThatThrownBy(()->underTest.save(user))
                .isInstanceOf(DataIntegrityViolationException.class)
                .withFailMessage("not-null property references a null or transient value : com.ezzahi.app.user.User.email");
    }
    @Test
    void itShouldNoteSaveTheUserWhenTheEmailIsExist() {
        //given
        String userEmail = "hamzaezzahi@gmail.com";
        User user1 = User.builder()
                .name("hamza")
                .email(userEmail)
                .password("password")
                .role(USER)
                .build();
        User user2 = User.builder()
                .id(100)
                .name("hamza")
                .email(userEmail)
                .password("password")
                .role(USER)
                .build();
        underTest.saveAndFlush(user1);
        //when
        //then
        assertThatThrownBy(()->underTest.saveAndFlush(user2))
                .isInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void isShouldNotSaveTheUserWhenTheNameIsNull() {
        //given
        User user = User.builder()
                .name(null)
                .email("hamza@ezzahi.com")
                .password("password")
                .role(USER)
                .build();
        //when
        //then
        assertThatThrownBy(()->underTest.save(user))
                .isInstanceOf(DataIntegrityViolationException.class)
                .withFailMessage("not-null property references a null or transient value : com.ezzahi.app.user.User.name");
    }

    @Test
    void isShouldNotSaveTheUserWhenThePasswdIsNull() {
        //given
        User user = User.builder()
                .name("hamza")
                .email("hamza@ezzahi.com")
                .password(null)
                .role(USER)
                .build();
        //when
        //then
        assertThatThrownBy(()->underTest.save(user))
                .isInstanceOf(DataIntegrityViolationException.class)
                .withFailMessage("not-null property references a null or transient value : com.ezzahi.app.user.User.password");
    }
    @Test
    void isShouldNotSaveTheUserWhenTheRoleIsNull() {
        //given
        User user = User.builder()
                .name("hamza")
                .email("hamza@ezzahi.com")
                .password("password")
                .role(null)
                .build();
        //when
        //then
        assertThatThrownBy(()->underTest.save(user))
                .isInstanceOf(DataIntegrityViolationException.class)
                .withFailMessage("not-null property references a null or transient value : com.ezzahi.app.user.User.role");
    }

}
