package com.ezzahi.app.util;

import com.ezzahi.app.util.EmailValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;


public class EmailValidatorTest {

    private EmailValidator underTest;
    @BeforeEach
    void setUp(){
        underTest = new EmailValidator();
    }
    @ParameterizedTest
    @ValueSource(strings = {
            "test@example.com",
            "user123@email.co",
            "john.doe@gmail.com",
            "alice.smith@yahoo.com"
    })
    void isShouldValidateTheEmail(String email) {
        assertThat(underTest.validateEmail(email)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "invalid.email",
            "user123@",
            "john.doe@.com",
            "alice.smith@domain"
    })
    void isShouldNotValidateTheEmail(String email) {
        assertThat(underTest.validateEmail(email)).isFalse();
    }
}
