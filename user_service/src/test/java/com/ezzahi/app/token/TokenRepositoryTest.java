package com.ezzahi.app.token;

import com.ezzahi.app.token.Token;
import com.ezzahi.app.token.TokenRepository;
import com.ezzahi.app.user.User;
import com.ezzahi.app.user.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static com.ezzahi.app.user.Role.USER;
import static org.assertj.core.api.Assertions.assertThat;
@DataJpaTest() // very impotant
public class TokenRepositoryTest {
    @Autowired
    private TokenRepository underTest;
    @Autowired
    private UserRepository userRepository;

    @Test
    void isShouldFindAllValidTokenByUserIfUserExist() {
        //given
        User user = User.builder()
                .id(1)
                .name("gamza")
                .email("hamza@hamza.com")
                .password("password")
                .role(USER)
                .build();
        userRepository.save(user);
        Token token1 = Token.builder()
                .token("token1")
                .revoked(false)
                .expired(false)
                .user(user)
                .build();
        Token token2 = Token.builder()
                .token("token2")
                .revoked(false)
                .expired(false)
                .user(user)
                .build();
        underTest.save(token1);
        underTest.save(token2);
        //when
        Optional<List<Token>> allValidTokenByUser = underTest.findAllValidTokenByUser(user.getId());
        //then
        assertThat(allValidTokenByUser).isPresent().hasValueSatisfying(
                tokenList->{
                    assertThat(tokenList)
                            .isNotEmpty()
                            .allMatch(token -> token.getUser().getId() == user.getId()
                                    && !token.isExpired() && !token.isRevoked()
                            );
                }
        );
    }
    @Test
    void isShouldNotFindAllValidTokenByUserIfUserExist() {
        //given
        //when
        //then
        assertThat(underTest.findAllValidTokenByUser(-1))
                .isPresent()
                .hasValueSatisfying(
                tokenList-> tokenList.isEmpty()
        );
    }
}
