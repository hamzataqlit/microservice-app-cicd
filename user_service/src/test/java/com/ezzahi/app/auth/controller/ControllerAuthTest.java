package com.ezzahi.app.auth.controller;

import com.ezzahi.app.UserService;
import com.ezzahi.app.auth.AuthenticationRequest;
import com.ezzahi.app.auth.AuthenticationResponse;
import com.ezzahi.app.auth.RegisterRequest;
import com.ezzahi.app.auth.service.AuthenticationService;
import com.ezzahi.app.config.ApplicationConfig;
import com.ezzahi.app.user.User;
import com.ezzahi.app.user.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Objects;

import static com.ezzahi.app.user.Role.USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)

public class ControllerAuthTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;



    @Test
    void shouldTestRegidterSuccess() throws Exception {
        //given
        RegisterRequest registerRequest = RegisterRequest.builder()
                .name("hamza")
                .password("hamza123")
                .email("hamzaezzahi199@gmail.com")
                .build();
        //when
        ResultActions userRegResultActions = mockMvc.perform(post("/api/v1/user/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(registerRequest))));
        //then
        userRegResultActions.andExpect(status().isOk());

    }

    @Test
    void shouldRegisterFailure() throws Exception {
        //given
        RegisterRequest registerRequest = RegisterRequest.builder()
                .name("hamza")
                .password("hamza123")
                .email("invalid email")
                .build();
        //when
        ResultActions userRegResultActions = mockMvc.perform(post("/api/v1/user/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(registerRequest))));
        //then
        userRegResultActions.andExpect(status().isBadRequest());

    }


    @Test
    void shouldTestAuthSuccess() throws Exception {
        //given
        User user = User.builder()
                .name("hamza")
                .email("hamzaezzahi155@gmail.com")
                .password(passwordEncoder.encode("hamza123"))
                .role(USER)
                .build();
        userRepository.saveAndFlush(user);
        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .password("hamza123")
                .email(user.getEmail())
                .build();

        //when
        mockMvc.perform(post("/api/v1/user/auth/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(authenticationRequest))))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.refresh_token").exists());


    }

    @Test
    void shouldAuthFailure() throws Exception {
        //given
        User user = User.builder()
                .name("hamza")
                .email("hamzaezzahi177@gmail.com")
                .password("hamzapasswd")
                .role(USER)
                .build();
        userRepository.saveAndFlush(user);
        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .password("wrong_passwd")
                .email(user.getEmail())
                .build();
        //when
        mockMvc.perform(post("/api/v1/user/auth/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(authenticationRequest))))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("invalid credential"));;
        //then


    }

    private String objectToJson(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            fail("Failed to convert object to json");
            return null;
        }
    }
}
