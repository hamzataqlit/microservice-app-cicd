package com.ezzahi.app.auth.controller;

import com.ezzahi.app.auth.UpdateUserRrequest;
import com.ezzahi.app.config.JwtService;
import com.ezzahi.app.token.Token;
import com.ezzahi.app.token.TokenRepository;
import com.ezzahi.app.token.TokenType;
import com.ezzahi.app.user.User;
import com.ezzahi.app.user.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.runner.WebApplicationContextRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.access.SecurityConfig;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.security.Security;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.ezzahi.app.user.Role.USER;
import static org.springframework.test.util.AssertionErrors.fail;


@SpringBootTest(
        properties = {
                "spring.properties.javax.persistence.validation.mode = none"
        }
)
@AutoConfigureMockMvc

public class ControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JwtService jwtService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenRepository tokenRepository;

    private int count = -1;
    ControllerTest(){

    }


    @Test
    public void testGetAllUsers_Success() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                                                        .expired(false)
                                                        .revoked(false).build(),
                                                        User.builder()
                                                                .name("hamza")
                                                                .email("hamzaezzahi1@gmail.com")
                                                                .password("hamza123")
                                                                .role(USER)
                                                                .build());

        // Perform the request and assert the response
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/user/all")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testGetAllUsers_Unauthorized() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(true)
                .revoked(true).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi2@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/user/all")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNetworkAuthenticationRequired());
    }
    @Test
    public void testGetById_Success() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(false)
                .revoked(false).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi3@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());
        // Perform the request and assert the response
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/user/1")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testGetById_Unauthorized() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(true)
                .revoked(true).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi4@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/user/1")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNetworkAuthenticationRequired());
    }
    @Test
    public void testisExist_Success() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(false)
                .revoked(false).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi5@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());

        // Perform the request and assert the response
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/user/isExist")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testisExist_Unauthorized() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(true)
                .revoked(true).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi6@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/user/isExist")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON))

                .andExpect(MockMvcResultMatchers.status().isNetworkAuthenticationRequired());
    }
    @Test
    public void testUpdateUser_Success() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(false)
                .revoked(false).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi7@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());
        UpdateUserRrequest updateUserRrequest = UpdateUserRrequest.builder()
                .name("valide name")
                .email("hamza@gmail.com")
                .build();
        // Perform the request and assert the response
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/user/update")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectToJson(updateUserRrequest))))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testUpdateUser_Unauthorized() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(true)
                .revoked(true).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi8@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());
        UpdateUserRrequest updateUserRrequest = UpdateUserRrequest.builder()
                .name("valide name")
                .email("hamza@hamza.com")
                .build();
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/user/update")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectToJson(updateUserRrequest))))
                .andExpect(MockMvcResultMatchers.status().isNetworkAuthenticationRequired());
    }
    @Test
    public void testUpdateUser_invalidEmail() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(false)
                .revoked(false).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi9@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());
        UpdateUserRrequest updateUserRrequest = UpdateUserRrequest.builder()
                .name("valide name")
                .email("hamza@.com")
                .build();
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/user/update")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectToJson(updateUserRrequest))))
                .andExpect(MockMvcResultMatchers.status().isNetworkAuthenticationRequired());
    }
    @Test
    public void testUpdateUser_EmailAlreadyExist() throws Exception {
        String authHeader = "Bearer "+generateTestToken(Token.builder()
                .expired(false)
                .revoked(false).build(),
                User.builder()
                        .name("hamza")
                        .email("hamzaezzahi10@gmail.com")
                        .password("hamza123")
                        .role(USER)
                        .build());
        User user = User.builder()
                .name("hamza")
                .email("hamza@hamza.com")
                .password("hamza123")
                .role(USER)
                .build();
        userRepository.save(user);
        UpdateUserRrequest updateUserRrequest = UpdateUserRrequest.builder()
                .name("valide name")
                .email(user.getEmail())
                .build();
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/user/update")
                        .header("Authorization", authHeader)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectToJson(updateUserRrequest))))
                .andExpect(MockMvcResultMatchers.status().isNetworkAuthenticationRequired());
    }
    private String objectToJson(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            fail("Failed to convert object to json");
            return null;
        }
    }
    public String generateTestToken(Token token,User user) {

        userRepository.save(user);
        long expirationInMillis = 3600000; // 1 hour expiration
        Map<String, Object> claims = new HashMap<>();
        String tk = jwtService.buildToken(claims,user,expirationInMillis);
        token.setUser(user);
        token.setToken(tk);
        return tokenRepository.save(token).token;
    }
}
