package com.ezzahi.app.auth.service;

import com.ezzahi.app.auth.AuthenticationRequest;
import com.ezzahi.app.auth.AuthenticationResponse;
import com.ezzahi.app.auth.RegisterRequest;
import com.ezzahi.app.config.JwtService;
import com.ezzahi.app.token.Token;
import com.ezzahi.app.token.TokenRepository;
import com.ezzahi.app.user.Role;
import com.ezzahi.app.user.User;
import com.ezzahi.app.user.UserRepository;
import com.ezzahi.app.util.EmailValidator;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.OutputStream;
import java.util.NoSuchElementException;
import java.util.Optional;

import static com.ezzahi.app.user.Role.USER;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserAuthServiceTest {
    @Captor
    private ArgumentCaptor<User> userArgumentCaptor ;
    @Captor
    private ArgumentCaptor<Token> tokenArgumentCaptor ;
    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;
    @Mock
    private  PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);
    @Mock
    private  JwtService jwtService = mock(JwtService.class);
    @Mock
    private  TokenRepository tokenRepository = mock(TokenRepository.class);
    @Mock
    private  AuthenticationManager authenticationManager = mock(AuthenticationManager.class);
    @Mock
    private UserRepository userRepository = mock(UserRepository.class);
    @Mock
    private EmailValidator emailValidator = mock(EmailValidator.class);

    private AuthenticationService underTest;
    @BeforeEach
    void setUp(){
        underTest = new AuthenticationService(passwordEncoder,jwtService,userRepository
                ,tokenRepository,authenticationManager,emailValidator);
    }
    @Test
    void isShouldRegisterTheUser() {
        //given
        RegisterRequest registerRequest = RegisterRequest.builder()
                .name("")
                .email("")
                .password("")
                .build();
        //when
        when(emailValidator.validateEmail(registerRequest.getEmail())).thenReturn(true);
        when(userRepository.findByEmail(registerRequest.getEmail())).thenReturn(Optional.empty());

        underTest.register(registerRequest);
        //then
        then(userRepository).should()
                .save(userArgumentCaptor.capture());
        User capterdValue = userArgumentCaptor.getValue();
        assertThat(capterdValue.getEmail())
                .isEqualTo(registerRequest.getEmail());
        assertThat(capterdValue.getId()).isNotNull();

    }

    @Test
    void isShouldNotRegisterTheUserWhenEmailNotValide() {
        //given
        RegisterRequest registerRequest = RegisterRequest.builder()
                .name("")
                .email("")
                .password("")
                .build();
        when(emailValidator.validateEmail(registerRequest.getEmail())).thenReturn(false);
        //when
        //then
        assertThatThrownBy(()->underTest.register(registerRequest))
                .isInstanceOf(IllegalStateException.class)
                .withFailMessage("email not valide");

        then(userRepository).should(never()).save(ArgumentMatchers.any());


    }

    @Test
    void isShouldNotRegisterWhenTheUserExist() {
        //given
        RegisterRequest registerRequest = RegisterRequest.builder()
                .name("")
                .email("")
                .password("")
                .build();
        given(emailValidator.validateEmail(registerRequest.getEmail())).willReturn(true);
        given(userRepository.findByEmail(registerRequest.getEmail())).willReturn(Optional.of(new User()));
        //when
        //then
        assertThatThrownBy(()->underTest.register(registerRequest))
                .isInstanceOf(IllegalStateException.class)
                .withFailMessage("user already exist ");
        then(userRepository).should(never()).save(ArgumentMatchers.any());
    }

    @Test
    void shouldAuthenticate() {
        //given
        User user = User.builder()
                .name("hamza")
                .email("hamzaezzahi177@gmail.com")
                .password("hamza123")
                .role(USER)
                .build();
        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
        Authentication auth = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());
        given(userRepository.findByEmail(user.getEmail())).willReturn(Optional.of(user));
        given(authenticationManager.authenticate(any())).willReturn(auth);
        given(jwtService.generateToken(user)).willReturn("fake-token");
        given(jwtService.generateRefreshToken(user)).willReturn("fake-refresh-token");
        //when
        AuthenticationResponse authenticateResponse = underTest.authenticate(authenticationRequest);
        //then
        then(userRepository).should()
                .findByEmail(user.getEmail());
        then(jwtService).should()
                .generateToken(user);
        then(jwtService).should()
                .generateRefreshToken(user);
        then(tokenRepository).should()
                .save(tokenArgumentCaptor.capture());

        Token capterToken = tokenArgumentCaptor.getValue();
        assertThat(capterToken.getToken()).isEqualTo("fake-token");
        assertThat(capterToken.getUser()).usingRecursiveAssertion()
                        .isEqualTo(user);
        assertThat(authenticateResponse.getAccessToken()).isEqualTo("fake-token");
        assertThat(authenticateResponse.getRefreshToken()).isEqualTo("fake-refresh-token");
    }

    @Test
    void shouldNotAuthenticate() {
        //given
        User user = User.builder()
                .name("hamza")
                .email("hamzaezzahi177@gmail.com")
                .password("hamza123")
                .role(USER)
                .build();
        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
        AuthenticationException authenticationException = new BadCredentialsException("Bad credentials");
        given(authenticationManager.authenticate(any()))
                .willThrow(authenticationException);
        //when
        //then
        assertThatThrownBy(()->underTest.authenticate(authenticationRequest))
                .isInstanceOf(AuthenticationException.class)
                .withFailMessage("Bad credentials");
    }

    @Test
    void shouldValidateTheToken() {
        //given
        Token token = Token.builder()
                .expired(false)
                .revoked(false)
                .build();
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        //when
        //then
        assertThatNoException().isThrownBy(()->underTest.isTokenValide("header-----------"));
    }
    @Test
    void shouldNotValidateTheToken() {
        //given
        Token token = Token.builder()
                .expired(true)
                .revoked(true)
                .build();
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        //when
        //then
        assertThatThrownBy(()->underTest.isTokenValide("header-----------"))
                .isInstanceOf(IllegalStateException.class)
                .withFailMessage("invalid token");
    }

    @Test
    void shouldGenereteARefreshToken() throws IOException {
        //given
        String validToken = "Bearer validRefreshToken";
        User user = User.builder()
                .id(100)
                .name("hamza")
                .email("hamzaezzahi177@gmail.com")
                .password("hamza123")
                .role(USER)
                .build();
        // Mock the response output stream
        ServletOutputStream mockOutputStream = mock(ServletOutputStream.class);

        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(validToken);
        when(jwtService.extractUsername("validRefreshToken")).thenReturn(user.getEmail());
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
        when(jwtService.isTokenValid("validRefreshToken", user)).thenReturn(true);
        when(jwtService.generateToken(user)).thenReturn("newAccessToken");
        when(response.getOutputStream()).thenReturn(mockOutputStream);
        //when
        underTest.refreshToken(request, response);
        //then
        then(tokenRepository).should()
                .save(tokenArgumentCaptor.capture());
        Token capterToken = tokenArgumentCaptor.getValue();
        assertThat(capterToken.token).isEqualTo("newAccessToken");
        assertThat(capterToken.user).usingRecursiveAssertion()
                .isEqualTo(user);
    }

    @Test
    void shouldNotGenerateARefreshTokenWhenTheUserIsNull() {
        //given
        String validToken = "Bearer validRefreshToken";
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(validToken);
        when(jwtService.extractUsername("validRefreshToken")).thenReturn("userName");//when
        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        //then
        assertThatThrownBy(()->underTest.refreshToken(request,response))
                .isInstanceOf(NoSuchElementException.class);
        then(jwtService).shouldHaveNoMoreInteractions();
        then(tokenRepository).shouldHaveNoInteractions();

    }
    @Test
    void shouldNotGenerateARefreshTokenWhenTheHeaderIsNull() throws IOException {
        //given
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(null);        //when
        ;
        //then
        underTest.refreshToken(request,response);
        then(userRepository).shouldHaveNoInteractions();
        then(jwtService).shouldHaveNoInteractions();
        then(tokenRepository).shouldHaveNoInteractions();

    }
}
