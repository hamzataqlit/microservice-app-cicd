package com.ezzahi.app.auth.service;

import com.ezzahi.app.auth.UpdateUserRrequest;
import com.ezzahi.app.config.JwtService;
import com.ezzahi.app.token.Token;
import com.ezzahi.app.token.TokenRepository;
import com.ezzahi.app.user.User;
import com.ezzahi.app.user.UserRepository;
import com.ezzahi.app.util.EmailValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ezzahi.app.user.Role.USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Captor
    ArgumentCaptor<User> userArgumentCaptor ;
    @Mock
    private  TokenRepository tokenRepository = mock(TokenRepository.class);
    @Mock
    private  UserRepository userRepository = mock(UserRepository.class);
    @Mock
    private  JwtService jwtService = mock(JwtService.class);
    @Mock
    private  EmailValidator emailValidator = mock(EmailValidator.class);

    private ServiceUser underTest;

    @BeforeEach
    void setUp(){
        underTest = new ServiceUser(tokenRepository,userRepository,jwtService,emailValidator);
    }

    @Test
    void shouldGetAllUsers() {
        //given
        String header = "Bearer valid token";
        Token token = Token.builder()
                .token(header.substring(7))
                .revoked(false)
                .expired(false)
                .build();
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        //when
        underTest.getAllUsers(header);
        //then
    }
    @Test
    void shouldNotGetAllUsers() {
        String header = "Bearer valid token";
        Token token = Token.builder()
                .token(header.substring(7))
                .revoked(true)
                .expired(true)
                .build();
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        //when
        assertThatThrownBy(()->underTest.getAllUsers(header))
                .isInstanceOf(IllegalStateException.class)
                .withFailMessage("invalid token");
        //then
    }
    @Test
    void shouldGetUser() {
        //given
        String header = "Bearer valid token";
        Token token = Token.builder()
                .token(header.substring(7))
                .revoked(false)
                .expired(false)
                .build();
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        given(userRepository.findById(any())).willReturn(Optional.of(new User()));
        //when
        underTest.getUser(header,1);
        //then
    }
    @Test
    void shouldNotGetUser() {
        String header = "Bearer valid token";
        Token token = Token.builder()
                .token(header.substring(7))
                .revoked(true)
                .expired(true)
                .build();
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        //when
        assertThatThrownBy(()->underTest.getUser(header,1))
                .isInstanceOf(IllegalStateException.class)
                .withFailMessage("invalid token");
        //then
    }

    @Test
    void shouldUpdateUser() {
        //given
        String header = "Bearer valid token";
        User user = User.builder()
                .id(100)
                .name("hamza")
                .email("hamzaezzahi177@gmail.com")
                .password("hamza123")
                .role(USER)
                .build();
        UpdateUserRrequest updateUserRrequest = UpdateUserRrequest.builder()
                .email("new email")
                .name("new name")
                .build();
        Token token = Token.builder()
                .token(header.substring(7))
                .revoked(false)
                .expired(false)
                .user(user)
                .build();
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        given(emailValidator.validateEmail(any())).willReturn(true);
        given(userRepository.findByEmail(any())).willReturn(Optional.empty());
        given(userRepository.findById(any())).willReturn(Optional.of(user));
        given(jwtService.extractId(any())).willReturn(user.getId()+"");

        //when
        underTest.updateUser(header,updateUserRrequest);
        //then
        then(userRepository).should().findById(any());
        then(userRepository).should()
                .save(userArgumentCaptor.capture());
        User captorUser = userArgumentCaptor.getValue();
        assertThat(captorUser.getEmail()).isEqualTo(updateUserRrequest.getEmail());
        assertThat(captorUser.getName()).isEqualTo(updateUserRrequest.getName());

    }

    @Test
    void shouldNotUpdateTheUserWhenNewEmailNotValid() {
        //given
        UpdateUserRrequest updateUserRrequest = UpdateUserRrequest.builder()
                .email("new email")
                .name("new name")
                .build();
        String header = "Bearer valid token";
        given(emailValidator.validateEmail(any())).willReturn(false);
        //when
        assertThatThrownBy(()->underTest.updateUser(header,updateUserRrequest))
                .isInstanceOf(IllegalStateException.class)
                .withFailMessage("email not valide");
        //then
    }
    @Test
    void shouldNotUpdateTheUserWhentokenNotValid() {
        String header = "Bearer valid token";
        Token token = Token.builder()
                .token(header.substring(7))
                .revoked(true)
                .expired(true)
                .build();
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        //when
        assertThatThrownBy(()->underTest.getUser(header,1))
                .isInstanceOf(IllegalStateException.class)
                .withFailMessage("invalid token");
        //then
    }
    @Test
    void shouldNotUpdateTheUserWhenEmailAlreadyExistValid() {
        //given
        UpdateUserRrequest updateUserRrequest = UpdateUserRrequest.builder()
                .email("new email")
                .name("new name")
                .build();
        String header = "Bearer valid token";
        given(emailValidator.validateEmail(any())).willReturn(true);
        given(userRepository.findById(any())).willReturn(Optional.of(User.builder()
                        .id(1)
                .build()));
        given(userRepository.findByEmail(any())).willReturn(Optional.of(User.builder()
                .id(2)
                .build()));
        given(jwtService.extractId(any())).willReturn("1");

        //when
        assertThatThrownBy(()->underTest.updateUser(header,updateUserRrequest))
                .isInstanceOf(IllegalStateException.class)
                .withFailMessage("email already exist ")
                ;
        //then

    }

    @Test
    void shouldGetUserImage() {
        //given
        String header = "Bearer valid token";
        Token token = Token.builder()
                .token(header.substring(7))
                .revoked(false)
                .expired(false)
                .build();
        given(jwtService.extractId(any())).willReturn("1");
        given(userRepository.findById(any())).willReturn(Optional.of(new User()));
        given(tokenRepository.findByToken(any())).willReturn(Optional.of(token));
        //when
        underTest.getUserImage(header);
        //then
    }
    @Test
    void shouldNotGetUserImage() {
        //given
        String header = "Bearer valid token";
        Token token = Token.builder()
                .token(header.substring(7))
                .revoked(false)
                .expired(false)
                .build();
        given(jwtService.extractId(any())).willReturn("1");
        given(userRepository.findById(any())).willReturn(Optional.of(new User()));
        given(tokenRepository.findByToken(any())).willReturn(Optional.empty());
        //when
        underTest.getUserImage(header);
        //then
    }
}
