package com.ezzahi.app.user;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ezzahi.app.user.Permissions.*;


@RequiredArgsConstructor
public enum Role {
    // permissions of admin role
    ADMIN(Set.of(
            USER_READ,
            USER_WRITE,
            USER_UPADTE,
            USER_DELETE
    )),
    USER(Set.of(
            USER_READ
    ));


    @Getter
    private final Set<Permissions> permissions;



    public List<SimpleGrantedAuthority> getAuthorities() {
        var authorities = getPermissions()
                .stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return authorities;
    }
}
