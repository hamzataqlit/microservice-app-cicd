package com.ezzahi.app.user;

public enum Permissions {
    USER_READ("student:read"),
    USER_WRITE("student:write"),
    USER_UPADTE("course:read"),
    USER_DELETE("course:write"),
    ;
    private final String permissions;
    Permissions(String permissions) {
        this.permissions = permissions;
    }
    public String getPermission(){
        return permissions;


    }
}
