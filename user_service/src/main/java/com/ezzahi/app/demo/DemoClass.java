package com.ezzahi.app.demo;

import com.ezzahi.app.auth.service.AuthenticationService;
import com.ezzahi.app.token.TokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/test")
public class DemoClass {
    private final TokenRepository tokenRepository;
    private final AuthenticationService authenticationService;
    @Autowired

    public DemoClass(TokenRepository tokenRepository, AuthenticationService authenticationService) {
        this.tokenRepository = tokenRepository;

        this.authenticationService = authenticationService;
    }

    @GetMapping
    public ResponseEntity<String> test(HttpServletRequest request){
        authenticationService.test(request);
        return ResponseEntity.ok("200 ok");
    }
}
