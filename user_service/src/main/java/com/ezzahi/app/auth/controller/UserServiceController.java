package com.ezzahi.app.auth.controller;

import com.ezzahi.app.UserService;
import com.ezzahi.app.auth.UpdateUserRrequest;
import com.ezzahi.app.auth.service.ServiceUser;
import com.ezzahi.app.user.User;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/user")
public class UserServiceController {
    private final ServiceUser userService;
    @Autowired
    public UserServiceController(ServiceUser userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public ResponseEntity getAllUsers(@RequestHeader("Authorization") String authHeader){
        try {
            return ResponseEntity.ok(userService.getAllUsers(authHeader));
        }catch (IllegalStateException e){
            return ResponseEntity.status(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED).build();
        }
    }

    @GetMapping("/{userId}")
    public ResponseEntity getUserById(@RequestHeader("Authorization") String authHeader , @PathVariable("userId") int userId){
        try {
            return ResponseEntity.ok(userService.getUser(authHeader,userId));
        }catch (IllegalStateException e){
            return ResponseEntity.status(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED).build();
        }
    }

    @PutMapping("/update")
    public ResponseEntity updateUser(@RequestHeader("Authorization") String authHeader , @RequestBody UpdateUserRrequest request){
        try {
            return ResponseEntity.ok(userService.updateUser(authHeader,request));
        }catch (IllegalStateException e){
            return ResponseEntity.status(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED).build();
        }
    }
    @GetMapping("/isExist")
    public ResponseEntity<Boolean> isUserExist(@RequestHeader("Authorization") String authHeader ){
        try {
            return ResponseEntity.ok(userService.isUserExist(authHeader));
        }catch (IllegalStateException e){
            return ResponseEntity.status(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED).build();
        }

    }
    @PutMapping("/updateImage/imageName={imageName}")
    public ResponseEntity updateUserImage(@RequestHeader("Authorization") String authHeader,@PathVariable("imageName") String imageName){
        try {
            return ResponseEntity.ok(userService.updateUserImage(authHeader,imageName));
        }catch (IllegalStateException e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    @GetMapping("/api/v1/user/getImage")
    public ResponseEntity<String> getUserImage(@RequestHeader("Authorization") String authHeader ){
        try {
            return ResponseEntity.ok(userService.getUserImage(authHeader));
        }catch (IllegalStateException e){
            return ResponseEntity.status(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED).build();
        }

    }

}
