package com.ezzahi.app.auth.service;

import com.ezzahi.app.auth.UpdateUserRrequest;
import com.ezzahi.app.config.JwtService;
import com.ezzahi.app.token.TokenRepository;
import com.ezzahi.app.user.User;
import com.ezzahi.app.user.UserRepository;
import com.ezzahi.app.util.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.token.TokenService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceUser {
    private final TokenRepository tokenRepository;
    private final UserRepository userRepository;
    private final JwtService jwtService;
    private final EmailValidator emailValidator ;
    @Autowired
    public ServiceUser(TokenRepository tokenRepository, UserRepository userRepository, JwtService jwtService,EmailValidator emailValidator) {
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
        this.jwtService = jwtService;
        this.emailValidator = emailValidator;
    }

    public List<User> getAllUsers(String authHeader) {
        final String token = authHeader.substring(7);
        isTokenValid(token);
        return userRepository.findAll();
    }

    public User getUser(String authHeader,int userId) {
        final String token = authHeader.substring(7);
        isTokenValid(token);
        return userRepository.findById(userId).get();
    }

    public User updateUser(String authHeader, UpdateUserRrequest request) {
        final String token = authHeader.substring(7);
        isTokenValid(token);
        // check if email is valid
        if(!emailValidator.validateEmail(request.getEmail())){
            throw new IllegalStateException("email not valide");
        }
        User user = userRepository.findById(Integer.parseInt(jwtService.extractId(token))).get();
        Optional<User> u = userRepository.findByEmail(request.getEmail());
        //check if email exist
        if(u.isPresent() && u.get().getId() != user.getId()){
            throw new IllegalStateException("email already exist ");
        }
        user.setName(request.getName());
        user.setEmail(request.getEmail());
        userRepository.save(user);
        return user;
    }

    public boolean isUserExist(String authHeader) {
        System.out.println(authHeader +"is exist");
        final String token = authHeader.substring(7);
        isTokenValid(token);
        return userRepository.findById(Integer.parseInt(jwtService.extractId(token))).isPresent();

    }

    public boolean updateUserImage(String authHeader,String imageName) {
        System.out.println(authHeader +"is exist");
        final String token = authHeader.substring(7);
        try{
            User user = userRepository.findById(Integer.parseInt(jwtService.extractId(token))).get();
            user.setImage(imageName);
            userRepository.save(user);
            return true;
        }catch (Exception e){
            throw new IllegalStateException("iamge not saved");
        }

    }

    public String getUserImage(String authHeader) {
        final String token = authHeader.substring(7);
        try{
            User user = userRepository.findById(Integer.parseInt(jwtService.extractId(token))).get();
            isTokenValid(token);
            return user.getImage();
        }catch (Exception e){
            throw new IllegalStateException("image not found");
        }
    }

    private void isTokenValid(String token) {
        tokenRepository.findByToken(token).map(t->{
            if (t.isRevoked() || t.isExpired()) {
                throw new IllegalStateException("invalid token");
            }
            return null;
        });
    }
}
