package com.ezzahi.app.auth.controller;

import com.ezzahi.app.ImageRequest;
import com.ezzahi.app.auth.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/v1/user/img")
public class ImageController {
    private final ImageService imageService;
    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping(
            path = "/upload",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE, //This specifies that the method expects the request content to be in the multipart/form-data
            produces = MediaType.APPLICATION_JSON_VALUE // This indicates that the response from the method will be in the JSON format
    )
    public ResponseEntity uploadUserImage(@RequestParam("file") MultipartFile file,
                                          @RequestHeader("Authorization") String token){
        try {
            Map<String,String> response = new HashMap<>();
            response.put("image",imageService.uploadUserImage(file,token));
            return ResponseEntity.ok(response);
        }catch (IllegalStateException e){
            Map<String,String> response = new HashMap<>();
            response.put("message","faild to upload");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

//    @GetMapping(
//            path = "/download/{imageName}"
//            //produces = MediaType.MULTIPART_FORM_DATA_VALUE //This specifies that the method expects the request content to be in the multipart/form-data
//            //produces = MediaType.APPLICATION_JSON_VALUE // This indicates that the response from the method will be in the JSON format
//    )
//    public byte[] downloadUserImage( @RequestHeader("Authorization") String token,@PathVariable("imageName") String imageName) throws IOException {
//        return profileImageService.downloadUserImage(token,imageName);
//    }

}
