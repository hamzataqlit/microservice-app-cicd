package com.ezzahi.app.auth.controller;

import com.ezzahi.app.auth.service.AuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/tokenValidator")
public class ValidationController {
    private final AuthenticationService service;

    public ValidationController(AuthenticationService service) {
        this.service = service;
    }


    @GetMapping
    ResponseEntity validateTheToken(@RequestHeader("Authorization") String header){
        try {
            service.isTokenValide(header);
            return ResponseEntity.ok("you can get your response :) ");
        }catch (IllegalStateException e){
            return ResponseEntity.status(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED).body("error");

        }

    }
}
