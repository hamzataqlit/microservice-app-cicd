package com.ezzahi.app.auth.controller;

import com.ezzahi.app.auth.AuthenticationRequest;
import com.ezzahi.app.auth.AuthenticationResponse;
import com.ezzahi.app.auth.service.AuthenticationService;
import com.ezzahi.app.auth.RegisterRequest;
import com.ezzahi.app.auth.RegisterResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("api/v1/user/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService service;

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> register(@RequestBody RegisterRequest registerRequest){
        try {
            return ResponseEntity.ok(service.register(registerRequest));
        }catch (IllegalStateException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(RegisterResponse.builder()
                            .message(e.getMessage())
                            .build());
        }


    }

    @PostMapping("/authenticate")
    public ResponseEntity login(@RequestBody AuthenticationRequest authenticationRequest){
        try {
            AuthenticationResponse authenticate = service.authenticate(authenticationRequest);
            return ResponseEntity.ok(authenticate);
        }catch (AuthenticationException  e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(RegisterResponse.builder()
                            .message("invalid credential")
                            .build());
        }
    }

    @PostMapping("/refresh-token")
    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        service.refreshToken(request, response);
    }

}
