package com.ezzahi.app.auth.service;

import com.ezzahi.app.auth.AuthenticationRequest;
import com.ezzahi.app.auth.AuthenticationResponse;
import com.ezzahi.app.auth.RegisterRequest;
import com.ezzahi.app.auth.RegisterResponse;
import com.ezzahi.app.config.JwtService;
import com.ezzahi.app.token.Token;
import com.ezzahi.app.token.TokenRepository;
import com.ezzahi.app.token.TokenType;
import com.ezzahi.app.user.Role;
import com.ezzahi.app.user.User;
import com.ezzahi.app.user.UserRepository;
import com.ezzahi.app.util.EmailValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Data
@AllArgsConstructor
@Service
public class AuthenticationService {
    @Autowired
    private  PasswordEncoder passwordEncoder;
    @Autowired
    private  JwtService jwtService;
    @Autowired
    private  UserRepository userRepository;
    @Autowired
    private  TokenRepository tokenRepository;
    @Autowired
    private  AuthenticationManager authenticationManager;
    @Autowired
    private  EmailValidator emailValidator ;

    public RegisterResponse register(RegisterRequest request) {
        // check if email is valid
        if(!emailValidator.validateEmail(request.getEmail())){
            throw new IllegalStateException("email not valide");
        }
        //check if user exist
        if(userRepository.findByEmail(request.getEmail()).isPresent()){
            throw new IllegalStateException("user already exist ");
        }
        // save the user
        User user = User.builder()
                .name(request.getName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .build();
        userRepository.save(user);

        return RegisterResponse.builder()
                .message("user created successfully")
                .build();

    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        // check user credential
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        // thats mean that the user credential is correct
        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        // save the token
        Token token = Token.builder()
                .token(jwtToken)
                .revoked(false)
                .expired(false)
                .tokenType(TokenType.BEARER)
                .user(user)
                .build();
        tokenRepository.save(token);
        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }
    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.get().forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens.get());
    }
    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String userEmail;
        if (authHeader == null ||!authHeader.startsWith("Bearer ")) {
            return;
        }
        refreshToken = authHeader.substring(7);
        userEmail = jwtService.extractUsername(refreshToken);
        if (userEmail != null) {
            var user = userRepository.findByEmail(userEmail)
                    .orElseThrow();
            if (jwtService.isTokenValid(refreshToken, user)) {
                var accessToken = jwtService.generateToken(user);
                revokeAllUserTokens(user);
                Token token = Token.builder()
                        .token(accessToken)
                        .revoked(false)
                        .expired(false)
                        .tokenType(TokenType.BEARER)
                        .user(user)
                        .build();
                tokenRepository.save(token);
                var authResponse = AuthenticationResponse.builder()
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build();
                new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
            }
        }
}

    public void test(HttpServletRequest request) {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String token = authHeader.substring(7);
        tokenRepository.findByToken(token).map(t->{
            if (t.isRevoked() || t.isExpired()) {
                throw  new IllegalStateException("invalid token");
            }
            System.out.println(t.id);
            return null;
        });
    }

    public void isTokenValide(String header) {
        final String token = header.substring(7);
        tokenRepository.findByToken(token).map(t->{
            if (t.isRevoked() || t.isExpired()) {
                throw new IllegalStateException("invalid token");
            }
            return null;
        });
        return;
    }
}
