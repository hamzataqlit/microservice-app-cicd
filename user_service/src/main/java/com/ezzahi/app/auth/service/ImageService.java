package com.ezzahi.app.auth.service;
import com.ezzahi.app.ImageRequest;
import com.ezzahi.app.token.Token;
import com.ezzahi.app.token.TokenRepository;
import com.ezzahi.app.user.User;
import com.ezzahi.app.user.UserRepository;
import feign.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.*;
import static org.apache.http.entity.ContentType.*;
@Service
@Slf4j
public class ImageService {
    private final KafkaTemplate<String, ImageRequest> producer;
    private final TokenRepository tokenRepository;
    private final UserRepository userRepository;



    public ImageService(KafkaTemplate<String, ImageRequest> producer,
                        TokenRepository tokenRepository,UserRepository userRepository) {
        this.producer = producer;
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
    }

    public String uploadUserImage(MultipartFile file, String authHeader)  {
        final String userToken = authHeader.substring(7);
        isTokenValid(userToken);
        // ctr alt m
        // 1. check if image not empty
        if(file.isEmpty()){
            throw new IllegalStateException(("empty file !"));
        }
        // 2. check if file is an image
        if(!Arrays.asList(IMAGE_JPEG.getMimeType(),IMAGE_PNG.getMimeType(),IMAGE_GIF.getMimeType()).contains(file.getContentType())) {
            throw new IllegalStateException(("file must be an image !"));
        }
        // 3. checkif the user is exist
        Optional<Token> token = tokenRepository.findByToken(userToken);

        // 4. Grab some metadata from file any
        Map<String,String> metadata = new HashMap<>();
        metadata.put("content-type",file.getContentType());
        metadata.put("content-length",String.valueOf(file.getSize()));
        // 5. store the image in s3 storage and update the image link of the user

        String fileName = String.format("%s-%s",file.getOriginalFilename(), UUID.randomUUID());
        try{
            ImageRequest imageRequest = ImageRequest.builder()
                    .metadata(metadata)
                    .fileName(fileName)
                    .data(file.getBytes())
                    .path("")
                    .build();
            producer.send("image-topic",imageRequest);
            //fileStore.save(path,fileName, Optional.of(metadata),file.getInputStream());
            // TODO: apdate user Image !!
            //user.setImageLink(fileName);
            User user = token.get().getUser();
            user.setImage(fileName);
            userRepository.save(user);
            return fileName;
        }catch (IOException e){
            throw new IllegalStateException("somthing get wrong");
        }
    }
    private void isTokenValid(String token) {
        tokenRepository.findByToken(token).map(t->{
            if (t.isRevoked() || t.isExpired()) {
                throw new IllegalStateException("invalid token");
            }
            return null;
        });
    }
}
