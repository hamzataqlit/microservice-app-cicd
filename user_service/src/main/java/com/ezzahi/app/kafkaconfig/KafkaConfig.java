package com.ezzahi.app.kafkaconfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;


@Configuration
public class KafkaConfig {

    @Bean
    public NewTopic imagesTopic(){
        return TopicBuilder.name("image-topic")
                .partitions(3)
                .build();
    }
}
