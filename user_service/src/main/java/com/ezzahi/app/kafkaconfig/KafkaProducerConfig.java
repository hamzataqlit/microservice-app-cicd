package com.ezzahi.app.kafkaconfig;
import com.ezzahi.app.ImageRequest;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;
@Configuration
public class KafkaProducerConfig {
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;
    public Map<String,String> imageProducerConfig(){
        Map<String,String> properties = new HashMap<>();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServers);
        properties.put(ProducerConfig.CLIENT_ID_CONFIG,"images_producer");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class.getName());
        return properties;
    }

    @Bean
    public ProducerFactory<String, ImageRequest> imageProducerFactory(){
        return new DefaultKafkaProducerFactory(imageProducerConfig());
    }

    @Bean
    public KafkaTemplate<String,ImageRequest> imagesKafkaTemplate(ProducerFactory<String,ImageRequest> producerFactory){
        return new KafkaTemplate<>(producerFactory);
    }
}
