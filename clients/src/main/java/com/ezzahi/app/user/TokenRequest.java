package com.ezzahi.app.user;

import lombok.Builder;

@Builder
public record TokenRequest (
        String token,
        String userId
        //
){
}
