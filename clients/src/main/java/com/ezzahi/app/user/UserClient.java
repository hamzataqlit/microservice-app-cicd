package com.ezzahi.app.user;

import feign.Headers;
import feign.Param;
import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient(name = "user", url = "${ep.client.user.url}")
public interface UserClient {
        @GetMapping(path = "api/v1/user/isExist")
        Response isUserExist(@RequestHeader("Authorization") String auth);
        @PutMapping(path = "api/v1/user/updateImage/imageName={imageName}")
        Response updateUserImage(@RequestHeader("Authorization") String auth,@PathVariable("imageName") String imageName);
        @GetMapping(path = "api/v1/user/getImage")
        Response getUserImage(@RequestHeader("Authorization") String auth);


}
