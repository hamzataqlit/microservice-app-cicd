package com.ezzahi.app.chat;

public enum MessageType {

    CHAT,
    JOIN,
    LEAVE
}
