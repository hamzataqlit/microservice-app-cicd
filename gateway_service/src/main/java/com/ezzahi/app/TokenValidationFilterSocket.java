package com.ezzahi.app;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
//
public class TokenValidationFilterSocket implements GatewayFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String token = extractTokenFromRequest(exchange.getRequest());
        WebClient client = WebClient.create("http://localhost:2001");
        Mono<String> result = client.get()
                .uri("/api/v1/tokenValidator")
                .header("Authorization", "Bearer "+token)
                .retrieve()
                .bodyToMono(String.class);

        // Process the response asynchronously
        return result.flatMap(responseEntity -> {
                // Check the response body for error conditions
                if (responseEntity != null && !responseEntity.contains("error")) {
                    // Handle the error case
                    System.out.println(responseEntity);
                    return chain.filter(exchange);
                }
                exchange.getResponse().setStatusCode(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);
                return exchange.getResponse().setComplete();
        }).onErrorResume(throwable -> {
            // Handle errors if needed
            System.out.println(throwable.getMessage());
            exchange.getResponse().setStatusCode(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);
            return exchange.getResponse().setComplete();
        });
    }


    private String extractTokenFromRequest(ServerHttpRequest request) {
        String authHeader = request.getQueryParams().getFirst("token");
        System.out.println(authHeader);
        if (authHeader != null ) {
            return authHeader; // Remove "Bearer " prefix to get the token
        }
        return null; // Token not found in the header
    }





}
