package com.ezzahi.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfiguration {

    private final TokenValidationFilter tokenValidationFilter;
    private final TokenValidationFilterSocket tokenValidationFilterSocket;
    @Value("${ep.user.url}")
    private String userServiceUrl;
    @Value("${ep.image.url}")
    private String imageServiceUrl;
    @Value("${ep.chat.url}")
    private String chatSericeUrl;

    @Autowired
    public FilterConfiguration(TokenValidationFilter tokenValidationFilter, TokenValidationFilterSocket tokenValidationFilterSocket) {
        this.tokenValidationFilter = tokenValidationFilter;

        this.tokenValidationFilterSocket = tokenValidationFilterSocket;
    }


//

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {

        return builder.routes()
                .route("user", r -> r.path("/api/v1/user/**")
                        .uri(userServiceUrl))
                .route("image", r -> r.path("/api/v1/image/**")
                        .filters((f)-> f.filter(tokenValidationFilter))
                        .uri(imageServiceUrl))
                .route(p -> p.path("/ws-connect/**")
                        .uri(chatSericeUrl))
                .route(p -> p.path("/web-connect/**")
                        .uri(chatSericeUrl))
                .build();
    }





}
