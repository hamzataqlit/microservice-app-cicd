package com.ezzahi.app.kafkaconfig;
import com.ezzahi.app.ImageRequest;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;
@Configuration
public class KafkaConsumerConfig {
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;


    public Map<String,String> imagesConcumerConfig(){
        Map<String,String> properties = new HashMap<>();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServers);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG,"images_consumer_group");
        return properties;
    }

    @Bean public ConsumerFactory<String, ImageRequest> imagesConsumerFactory(){
        JsonDeserializer jsonDeserializer = new JsonDeserializer();
        jsonDeserializer.addTrustedPackages("com.ezzahi.app");
        return new DefaultKafkaConsumerFactory(
                imagesConcumerConfig(),
                new StringDeserializer(),
                jsonDeserializer);
    }



    @Bean
    public KafkaListenerContainerFactory<
            ConcurrentMessageListenerContainer<String,ImageRequest>> imagesFactory (
            ConsumerFactory<String,ImageRequest> consumerFactory
    ){
        ConcurrentKafkaListenerContainerFactory<String,ImageRequest> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }
}
