package com.ezzahi.app.kafkaconfig;

import com.ezzahi.app.ImageRequest;
import com.ezzahi.app.imageservice.ProfileImageService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class KafkaListeners {
    private final ProfileImageService imageService;

    public KafkaListeners(ProfileImageService imageService) {
        this.imageService = imageService;
    }

    @KafkaListener(
            topics = "image-topic",
            groupId = "images_consumer_group",
            containerFactory = "imagesFactory"
    )
    void imagesLisener(ImageRequest imageRequest) throws IOException {
        imageService.uploadUserImage(imageRequest);
    }
}
