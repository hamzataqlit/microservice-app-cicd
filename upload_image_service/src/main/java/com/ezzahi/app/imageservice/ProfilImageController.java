package com.ezzahi.app.imageservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/image")
public class ProfilImageController {
    private final ProfileImageService profileImageService;
    @Autowired

    public ProfilImageController(ProfileImageService profileImageService) {
        this.profileImageService = profileImageService;
    }





    @GetMapping(
            path = "/download/{imageName}"
            //produces = MediaType.MULTIPART_FORM_DATA_VALUE //This specifies that the method expects the request content to be in the multipart/form-data
            //produces = MediaType.APPLICATION_JSON_VALUE // This indicates that the response from the method will be in the JSON format
    )
    public byte[] downloadUserImage( @PathVariable("imageName") String imageName) throws IOException {
        return profileImageService.downloadUserImage(imageName);
    }
}
