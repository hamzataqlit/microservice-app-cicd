package com.ezzahi.app.imageservice;

import com.ezzahi.app.ImageRequest;
import com.ezzahi.app.filestore.FileStore;
import com.ezzahi.app.user.UserClient;
import feign.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import static com.ezzahi.app.bucket.BucketName.PROFIL_IMAGE;
import static org.apache.http.entity.ContentType.*;

@Service
@AllArgsConstructor
@Slf4j
public class ProfileImageService {

    // use interface for implement something
    private final FileStore fileStore;
    private final UserClient userClient;




    public void uploadUserImage(ImageRequest imageRequest)  {
        String path = String.format("%s",PROFIL_IMAGE.getBucketName());
        try{
            InputStream inputStream = new ByteArrayInputStream(imageRequest.data());
            fileStore.save(path,imageRequest.fileName(),Optional.of(imageRequest.metadata()),inputStream);
        }catch (Exception e){
            throw new IllegalStateException("sothing get wrong");
        }
    }

    public byte[] downloadUserImage(String imageName) throws IOException {
        String path = String.format("%s",PROFIL_IMAGE.getBucketName());
//        Response isUserExist = userClient.isUserExist(token);
//        if(isUserExist.status() != 200){
//            throw new IllegalStateException("user not exist or the token have been changed");
//        }
//        Response response = userClient.getUserImage(token);
//        byte[] responseBodyBytes = response.body().asInputStream().readAllBytes();
//        String imagePath = new String(responseBodyBytes, StandardCharsets.UTF_8);
        return fileStore.download(path,imageName);

    }
}
