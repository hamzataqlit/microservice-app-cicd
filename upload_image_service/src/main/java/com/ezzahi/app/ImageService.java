package com.ezzahi.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@EnableFeignClients(
        basePackages = "com.ezzahi.app.user"
)

@PropertySources(
        @PropertySource("classpath:clients-${spring.profiles.active}.properties")

)
//
public class ImageService {

    public static void main(String[] args) {
        SpringApplication.run(ImageService.class, args);
    }

}
